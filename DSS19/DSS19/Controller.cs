﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Data;
using System.Drawing;
using System.IO;

namespace DSS19
{
    class Controller
    {
        Persistence persistence = new Persistence();
        private string dbPath;
        private string pythonScriptPath;
        private PythonRunner pyRunner;
        private string strCustomers;
        private static string arimaForecastScript = "arima_forecast.py";
        private static string chartOrdersScript = "chartOrders.py";
        private GAPclass GAP;

        public Controller(string dbPath, string pythonPath, string pythonScriptPath)
        {
            this.pythonScriptPath = pythonScriptPath;
            this.dbPath = dbPath;
            pyRunner = new PythonRunner(pythonPath, 20000);
            this.GAP = new GAPclass();
            persistence.connectionString = dbPath;
        }

        #region using EntityFramework
        //legge una stringa di codici clienti da graficare
        //howMany = numero di clienti di cui leggere la serie degli ordini
        internal string readRandomCustomerList(int howMany) 
        {
            this.strCustomers = persistence.readRandomCustomerList(dbPath, howMany);
            Trace.WriteLine($"Clienti: {strCustomers}");
            return strCustomers;
        }

        public bool isCustomer(string customer)
        {
            return persistence.isCustomerExisting(dbPath, customer);
        }

        public async Task<Bitmap> readCustomerOrdersChart() 
        {
            Trace.WriteLine("Getting orders chart ...");
            pythonScriptPath = Path.GetFullPath(pythonScriptPath);

            try
            {
                Bitmap bitmap = await pyRunner.getImageAsync(
                    pythonScriptPath,
                    chartOrdersScript,
                    pythonScriptPath,
                    dbPath,
                    strCustomers);
                return bitmap;
            }
            catch
            {
                Trace.WriteLine("error creating bitmap...");
                return null;
            }

        }

        internal async Task<string> getChartTextOutput(string customer) 
        {
            Trace.WriteLine("Getting strings from output:");
            pythonScriptPath = Path.GetFullPath(pythonScriptPath);

            try
            {
                string output = await pyRunner.getStringsAsync(
                    pythonScriptPath,
                    arimaForecastScript,
                    pythonScriptPath,
                    dbPath,
                    customer);

                int from = output.IndexOf("Actual");
                int to = output.IndexOf("b'");
                return output.Substring(from, to - from);
            }
            catch
            {
                return "Error getting strings...";
            }
        }

        public async Task<Bitmap> readArimaForecastChart(string customer) 
        {
            Trace.WriteLine("getting the arima forecast chart ...");
            pythonScriptPath = Path.GetFullPath(pythonScriptPath);

            try
            {
                Bitmap bitmap = await pyRunner.getImageAsync(
                    pythonScriptPath,
                    arimaForecastScript,
                    pythonScriptPath,
                    dbPath,
                    customer);
                return bitmap;
            }
            catch
            {
                Trace.WriteLine("error creating bitmap...");
                return null;
            }
        }

        public async Task<string> forecastAllCustomers(bool optimize) 
        {
            List<string> customers = persistence.getCustomerList(dbPath);
            
            for (int i = 0; i < customers.Count; i++)
            {
                string customer = "'" + customers[i] + "'";
                pythonScriptPath = Path.GetFullPath(pythonScriptPath);
                try
                {
                    string output = await pyRunner.getStringsAsync(
                        pythonScriptPath,
                        arimaForecastScript,
                        pythonScriptPath,
                        dbPath,
                        customer);

                    double fcast = this.setLastForecast(output);
                    Trace.WriteLine($"Forecast ordini cliente {customers[i]}: {fcast}");
                   
                    if(optimize) GAP.req[i] = (int)Math.Round(fcast);
                    
                }
                catch
                {
                    Trace.WriteLine("Error getting strings...");
                }
            }
            return String.Empty;
        }

        private double setLastForecast(string output)
        {
            string fcast = "";
            string[] lines = output.Split(new[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);

            foreach (string line in lines)
            {
                if (line.StartsWith("Actual"))
                {
                    fcast = line.Substring(line.LastIndexOf(" "));
                }
            }
            return Convert.ToDouble(fcast);
        }

        public async void optimizeGAP() 
        {
            persistence.readGAPinstance(this.dbPath, GAP);
            
            if (persistence.checkFileExist())
            {
                string[] txtData = persistence.readGAPFile();
                GAP.req = Array.ConvertAll<string, int>(txtData, new Converter<string, int>(i => int.Parse(i)));
            }
            else
            {
                await forecastAllCustomers(true);
                persistence.writeToFile(GAP.req);
                
            }
            double zub = GAP.simpleContruct();
            Trace.WriteLine($"Constructive, zub = {zub}");
            zub = GAP.opt10(GAP.costs);
            Trace.WriteLine($"Local Search, zub = {zub}");
            zub = GAP.tabuSearch(30, 1000);
            Trace.WriteLine($"Tabu Search, zub = {zub}");
        }
    }
    #endregion
}
