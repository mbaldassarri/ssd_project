﻿using System;
using System.Drawing;
using System.Windows.Forms;
using System.Diagnostics;
using System.Configuration;

namespace DSS19
{
    public partial class View : Form
    {
        Controller controller;
        TextBoxTraceListener _textBoxListener;

        private string pythonScriptPath;
        private string dbOrdiniPath;
        private string pythonPath;
        private Bitmap bmp;
        private static int randomMaxCustomers = 12;

        public View()
        {
            InitializeComponent();
            _textBoxListener = new TextBoxTraceListener(txtConsole);
            Trace.Listeners.Add(_textBoxListener);
 
            pythonScriptPath = ConfigurationManager.AppSettings["pythonScripts"];
            dbOrdiniPath = ConfigurationManager.AppSettings["dbOrdiniFile"];
            pythonPath = ConfigurationManager.AppSettings["pythonPath"];

            controller = new Controller(dbOrdiniPath, pythonPath, pythonScriptPath);
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            sender.ToString();
        }

        private async void BtnReadCustList_Click(object sender, EventArgs e)
        {
            controller.readRandomCustomerList(randomMaxCustomers);
            bmp = await controller.readCustomerOrdersChart();
            pictureBox.Image = bmp;
        }

        private async void BtnReadArimaForecastChart_Click(object sender, EventArgs e)
        {
            string customer = "'" + txtCustomer.Text + "'";
            if (controller.isCustomer(customer))
            {
                string output = await controller.getChartTextOutput(customer);
                Trace.WriteLine(output);
                bmp = await controller.readArimaForecastChart(customer);
                pictureBox.Image = bmp;
            } else
            {
                Trace.WriteLine("Wrong customer! Please try again inserting a customer using the text box!");
            }
        }

        private  async void BtnCustomersForecast_Click(object sender, EventArgs e)
        {
            await this.controller.forecastAllCustomers(false);
        }

        private void BtnOptimize_Click(object sender, EventArgs e)
        {
            controller.optimizeGAP(); 
        }

        private void ordersChartToolStripMenuItem_Click(object sender, EventArgs e)
        {
            BtnReadCustList_Click(sender, e);
        }

        private void sarimaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            BtnReadArimaForecastChart_Click(sender, e);
        }

        private async void forecastToolStripMenuItem_Click(object sender, EventArgs e)
        {
            await this.controller.forecastAllCustomers(false);
        }

        private void optimizeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            BtnOptimize_Click(sender, e);
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }

    public class TextBoxTraceListener : TraceListener
    {
        private TextBox _target;
        private StringSendDelegate _invokeWrite;
        public TextBoxTraceListener(TextBox target)
        {
            _target = target;
            _invokeWrite = new StringSendDelegate(SendString);
        }
        public override void Write(string message)
        { _target.Invoke(_invokeWrite, new object[] { message }); }
        public override void WriteLine(string message)
        { _target.Invoke(_invokeWrite, new object[] { message + Environment.NewLine }); }
        private delegate void StringSendDelegate(string message);
        private void SendString(string message)
        { _target.AppendText(message); }
    }
}
