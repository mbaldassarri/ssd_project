using System;
using System.Diagnostics;

namespace DSS19
{
    class GAPclass
    {
        public int numClienti;  // numero clienti
        public int numMagazzini;  // numero magazzini
        public double[,] costs;  // costi assegnamento
        public int[] req;    // richieste clienti
        public int[] amount;    // capacità magazzini

        public int[] sol, solbest;    // per ogni cliente, il suo magazzino
        public double zub, zlb; //zUpperBound, zLowerBound(<= al costo della soluzione ottima del problema)


        const double EPS = 0.0001;
        System.Random rnd = new Random(550);

        public GAPclass()
        {
            zub = double.MaxValue;
            zlb = double.MinValue;
        }

        public double simpleContruct()
        {
            int i, ii, j;
            int[] capleft = new int[amount.Length], ind = new int[numMagazzini];
            double[] dist = new double[numMagazzini];
            Array.Copy(amount, capleft, amount.Length);

            zub = 0;
            for (j = 0; j < numClienti; j++)
            {
                for (i = 0; i < numMagazzini; i++)
                {
                    dist[i] = costs[i, j];
                    ind[i] = i;
                }
                Array.Sort(dist, ind);
                ii = 0;
                while (ii < numMagazzini)
                {
                    i = ind[ii];
                    if (capleft[i] >= req[j])
                    {
                        sol[j] = i;
                        capleft[i] -= req[j];
                        zub += costs[i, j];
                        break;
                    }
                    ii++;
                }
                if (ii == numMagazzini)
                    Trace.WriteLine("[SimpleConstruct] Oops! ii=" + ii);
            }
            return zub;
        }

        public double opt10(double[,] c)
        {
            int[] capleft = new int[amount.Length];
            Array.Copy(amount, capleft, amount.Length);
            double z = 0;
            int i, isol, j;

            for (j = 0; j < numClienti; j++)
            {
                capleft[sol[j]] -= req[j];
                z += c[sol[j], j];
            }

        l0: for (j = 0; j < numClienti; j++)
            {
                isol = sol[j];
                for (i = 0; i < numMagazzini; i++)
                {
                    if (i == isol) continue;
                    if (c[i, j] < c[isol, j] && capleft[i] >= req[j])
                    {
                        sol[j] = i;
                        capleft[i] -= req[j];
                        capleft[isol] += req[j];
                        z -= (c[isol, j] - c[i, j]);
                        if (z < zub)
                        {
                            zub = z;
                            Trace.WriteLine("[1-0 opt] new zub " + zub);
                        }
                        goto l0;
                    }
                }
            }
            return zub;
        }

        public double tabuSearch(int tabuTenure, int iterations)
        {
            int i, j;
            int currentSolution;
            double zUpperBound;
            int[] capleft = new int[amount.Length];
            int[,] TL = new Int32[numMagazzini, numClienti];
            Array.Copy(amount, capleft, amount.Length);

            for (j = 0; j < numClienti; j++)
            {
                capleft[sol[j]] -= req[j];
            }

            zUpperBound = zub;

            Trace.WriteLine("Tabu Search Algorithm -> Start");

            // initialize array
            for (i = 0; i < numMagazzini; i++)
            {
                for (j = 0; j < numClienti; j++)
                {
                    TL[i, j] = int.MinValue;
                }
            }

            for (int index = 0; index < iterations; index++) {
                double deltaMax = int.MinValue;
                int imax = int.MinValue;
                int jmax = int.MinValue;
                for (j = 0; j < numClienti; j++)
                {
                    currentSolution = sol[j];
                    for (i = 0; i < numMagazzini; i++)
                    {
                        if (i == currentSolution) continue;
                        if ((costs[currentSolution, j] - costs[i, j]) > deltaMax && capleft[i] >= req[j] && (TL[i, j] + tabuTenure) < index)
                        {
                            imax = i;
                            jmax = j;
                            deltaMax = costs[currentSolution, j] - costs[i, j];
                        }
                    }
                }

                currentSolution = sol[jmax];
                sol[jmax] = imax;
                capleft[imax] -= req[jmax];
                capleft[currentSolution] += req[jmax];
                zUpperBound -= deltaMax;
                if (zUpperBound < zub)
                {
                    zub = zUpperBound;
                }
                TL[imax, jmax] = index;
                Trace.WriteLine("Tabu Search ->  zUpperBound " + zUpperBound + " index " + index + " deltamax " + deltaMax);
            }
            Trace.WriteLine("Tabu search Algorithm -> End ");
            return zub;
        }
    }
}
