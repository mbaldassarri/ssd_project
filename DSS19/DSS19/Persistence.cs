﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics;
using System.IO;

namespace DSS19
{
    class Persistence
    {
        public string connectionString;
        private static string GAPFileName = "GAPreq.dat";

        #region EntityFramework

        public List<string> getCustomerList(string dbpath)
        {
            List<string> customers = new List<string>();
            try
            {
                using (var context = new SQLiteDatabaseContext(dbpath))
                {
                    customers = context.Database.SqlQuery<string>(@"SELECT distinct customer from ordini").ToList();

                }
            }
            catch (Exception ex)
            {
                Trace.WriteLine($"Error: {ex.Message}");
            }
            return customers;
        }

        public bool isCustomerExisting(string dbpath, string customer)
        {
            List<string> lstClienti;
            try
            {
                using (var context = new SQLiteDatabaseContext(dbpath))
                {
                    lstClienti = context.Database.SqlQuery<string>(@"SELECT distinct customer from ordini where customer = " + customer).ToList();

                }
                return lstClienti.Count > 0 ? true : false;
            }
            catch (Exception ex)
            {
                Trace.WriteLine($"Error: {ex.Message}");
                return false;
            }
        }

        public string readRandomCustomerList(string dbpath, int numSerie)
        {
            List<string> lstClienti;
            string ret = "Error reading DB";
            try
            {
                using (var context = new SQLiteDatabaseContext(dbpath))
                {
                    lstClienti = context.Database.SqlQuery<string>("SELECT distinct customer from ordini order by random()").ToList();
                   
                }

                List<string> lstOutStrings = new List<string>();

                Random r = new Random(550);
                while (lstOutStrings.Count < numSerie)
                {
                    int randomIndex = r.Next(0, lstClienti.Count); //Choose a random object in the list
                    lstOutStrings.Add("'" + lstClienti[randomIndex] + "'"); //add it to the new, random list
                    lstClienti.RemoveAt(randomIndex); //remove to avoid duplicates
                }
                ret = string.Join(",", lstOutStrings);
            }
            catch (Exception ex)
            {
                Trace.WriteLine($"Error: {ex.Message}");
            }
            return ret;
        }

        // Reads GAP instance from the db
        public void readGAPinstance(string dbpath, GAPclass G)
        {
            int i, j;
            List<int> lstCap;
            List<double> lstCosts;

            try
            {
                using (var ctx = new SQLiteDatabaseContext(dbpath))
                {
                    lstCap = ctx.Database.SqlQuery<int>("SELECT cap from capacita").ToList();
                    G.numMagazzini = lstCap.Count();
                    G.amount = new int[G.numMagazzini];
                    for (i = 0; i < G.numMagazzini; i++)
                        G.amount[i] = lstCap[i];

                    lstCosts = ctx.Database.SqlQuery<double>("SELECT cost from costi").ToList();
                    G.numClienti = lstCosts.Count / G.numMagazzini;
                    G.costs = new double[G.numMagazzini, G.numClienti];
                    G.req = new int[G.numClienti];
                    G.sol = new int[G.numClienti];
                    G.solbest = new int[G.numClienti];
                    G.zub = Double.MaxValue;
                    G.zlb = Double.MinValue;

                    for (i = 0; i < G.numMagazzini; i++)
                        for (j = 0; j < G.numClienti; j++)
                            G.costs[i, j] = lstCosts[i * G.numClienti + j];

                    for (j = 0; j < G.numClienti; j++)
                        G.req[j] = -1;          // placeholder
                }
            }
            catch (Exception ex)
            {
                Trace.WriteLine("[readGAPinstance] Error:" + ex.Message);
            }

            Trace.WriteLine("Fine lettura dati istanza GAP");
        }

        internal bool checkFileExist()
        {
            return File.Exists(GAPFileName);
        }

        internal string[] readGAPFile()
        {
            return File.ReadAllLines(GAPFileName);
        }

        internal void writeToFile(int[] req)
        {
            File.WriteAllLines(GAPFileName, req.Select(x => x.ToString()));
        }
    }
    #endregion Entity Framework
}