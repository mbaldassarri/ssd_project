Per lanciare gli esempi devo crearmi un environment dedicato in Anaconda.

aprire Anaconda Prompt

> conda create -n <nome_environment> python=3.6.8
crea un nuovo ambiente di sviluppo nel caso in cui le librerie che devo installare avessero bisogno di una versione diversa di python (tensor flow ha bisogno di una versione < 4)
-n e' il nome da dare in questo caso ho scelto ssd ma posso mettere topolino

posso attivarla con:
> conda activate <nome_environment>
 
per installare nuove librerie appartenenti ad Anaconda scrivo:
>conda install numpy 

nel caso un pacchetto non sia presente in Anaconda (es pmdarima) usare direttamente pip sempre dopo aver attivato l'environment creato.
> pip install pmdarima

una volta completato su Anaconda scegliere l'environment nel menu a tendina in alto.