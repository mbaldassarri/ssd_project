# 1) ARIMA example
import os
import pandas as pd, matplotlib.pyplot as plt 
from statsmodels.tsa.arima_model import ARIMA
# Import data
os.chdir('/Users/marco/Documents/SSD_Project/SSD_Python/arima_example')
df = pd.read_csv('rawAirlinesPassengers.csv', usecols=[0], names=['value'], header=0)
# 1,1,2 ARIMA Model (p,d,q)
model = ARIMA(df.value, order=(1,1,2)) 
model_fit = model.fit(disp=0) 
print(model_fit.summary()) #viene definita l'equazione sui dati, qui viene costruito il modello applicabile ai nostri dati
# Plot residual errors
residuals = pd.DataFrame(model_fit.resid) #quello che resta dopo l'applicazione del modello. Si tratta del resto, che viene inserito in un data frame
fig, ax = plt.subplots(1,2) 
residuals.plot(title="Residuals", ax=ax[0]) 
residuals.plot(kind='kde', title='Density', ax=ax[1]) 
plt.show()




# 2) SARIMA example
import pandas as pd, numpy as np
import matplotlib.pyplot as plt
import os
os.chdir('/Users/marco/Documents/SSD_Project/SSD_Python/arima_example') 
df = pd.read_csv('esempio.csv') # dataframe (series) 
df.plot(x='anno-trim',y='sales')
plt.title('Sales', color='black')
plt._show()
#Preprocessing: log transform
npa = df['sales'].to_numpy() #prima trasform da colonna di data-frame ad array numpy. Potevo scrivere anche df.sales.to_numpy()
logdata = np.log(npa) #applico il log ad ogni valore di ogni cella dell'array
plt.plot(npa, color = 'blue', marker = "o") 
plt.plot(logdata, color = 'red', marker = "o") 
plt.title("numpy.log()") 
plt.xlabel("x");
plt.ylabel("logdata") 
plt.show()
#Autocorrelazione
from statsmodels.tsa.stattools import acf
df = pd.read_csv('esempio.csv')
diffdata = df['sales'].diff()
diffdata[0] = df['sales'][0] # reset 1st elem 
acfdata = acf(diffdata, unbiased=True, nlags=8) 
plt.bar(np.arange(len(acfdata)),acfdata) 
plt.show
# oppure lo faccio fare direttamente al modulo statsmodels
import statsmodels.api as sm 
sm.graphics.tsa.plot_acf(diffdata, lags=8) 
plt.show


# 2a)  SARIMA example 2 - faccio fare direttamente alle API a disposizione, commentare example1 prima di lanciarlo
#Preprocessing, log - diff transform
df = pd.read_csv('esempio.csv', header=0) 
df.set_index('anno-trim')

aSales = df['sales'].to_numpy()     # array of sales data   
logdata = np.log(aSales)            # log transform 
logdiff = pd.Series(logdata).diff() # logdiff transform

#Preprocessing, train and test set
cutpoint = int(0.7*len(logdiff)) 
train = logdiff[:cutpoint]
test = logdiff[cutpoint:]
#Postprocessing, reconstruction
train[0] = 0 # set first entry 
reconstruct = np.exp(np.r_[train,test].cumsum()+logdata[0])
#r_ ->merge: concatene i due array: train e test
#cumsum: devo togliere la differenzazione tramite una somma cumulata.
#aggiungo primo valore della serie logaritmica-> traslo di un valore in alto pari al primo log
#ora devo invertire il log -> exp
plt.plot(df.sales)
plt.plot(pd.Series(reconstruct))
plt.show


# 3) SARIMAX 
#densità -> cerca la migliore distribuzione interpolante che è quella arancione , diversa dalla gausssiana verda.
#non è significativo però perche è dati sono pochi quindi  uso Q-Q plot
#correlazioni cadono sull'area di non significativà quindi probabile sia rumore bianco
from statsmodels.tsa.statespace.sarimax import SARIMAX 
sarima_model = SARIMAX(df.sales, order=(0,2,2), seasonal_order=(0,1,0,4)) 
sfit = sarima_model.fit()
sfit.plot_diagnostics(figsize=(10, 6))
plt.show()


# 4) SARIMA
import pandas as pd, numpy as np
import matplotlib.pyplot as plt
import os
os.chdir('/Users/marco/Documents/SSD_Project/SSD_Python/arima_example') 
df = pd.read_csv('esempio.csv') # dataframe (series) 
df.plot(x='anno-trim',y='sales')
plt.title('Sales', color='black')
plt._show()

from statsmodels.tsa.statespace.sarimax import SARIMAX 
ds = df.sales
sarima_model = SARIMAX(df.sales, order=(0,2,2), seasonal_order=(0,1,0,4)) 
sfit = sarima_model.fit()
sfit.plot_diagnostics(figsize=(10, 6))
plt.show()
# predizioni in-sample
ypred = sfit.predict(start=0, end=len(ds))
plt.plot(ds.values)
plt.plot(ypred)
plt.xlabel('time')
plt.ylabel('sales')
plt.show()

forewrap = sfit.get_forecast(steps=4) 
forecast_ci = forewrap.conf_int() 
forecast_val = forewrap.predicted_mean 
plt.plot(ds.values) 
plt.fill_between(forecast_ci.index,
forecast_ci.iloc[:, 0],
forecast_ci.iloc[:, 1], color='k', alpha=.25) 
plt.plot(forecast_val)
plt.xlabel('time');
plt.ylabel('sales') 
plt.show()

#5)
import pmdarima as pm # pip install pmdarima
import os
import pandas as pd
os.chdir('/Users/marco/Documents/SSD_Project/SSD_Python/decomposition_example') 
df = pd.read_csv('FilRouge.csv', names=['sales'], header=0) 
ds = df.sales
model = pm.auto_arima(ds.values, start_p=1, start_q=1,
                        test='adf', max_p=3, max_q=3, m=4, 
                        start_P=0, seasonal=True,d=None,
                        D=1, trace=True, error_action='ignore', 
                        suppress_warnings=True, stepwise=True) # False full grid
print(model.summary())
morder = model.order
mseasorder = model.seasonal_order
fitted = model.fit(ds)
yfore = fitted.predict(n_periods=4) # forecast 
ypred = fitted.predict_in_sample() 
plt.plot(ds.values)
plt.plot(ypred)
plt.plot([None for i in ypred] + [x for x in yfore]) 
plt.xlabel('time');
plt.ylabel('sales')
plt.show()

