import os
import pandas as pd
from matplotlib import pyplot as plot
from statsmodels.tsa.seasonal import seasonal_decompose
os.chdir('/Users/marco/Documents/SSD_Project/SSD_Python/decomposition_example') 
plot.rcParams['figure.figsize'] = (10.0, 6.0)
series = pd.read_csv('BoxJenkins.csv',usecols=['Passengers'], header=0)
result = seasonal_decompose(series, model='multiplicative',freq=12)
result.plot()
plot.show()


#Esempio di Train-Test su BoxJenkins
import pandas as pd, os
from matplotlib import pyplot
os.chdir('/Users/marco/Documents/SSD_Project/SSD_Python/decomposition_example')
series = pd.read_csv('BoxJenkins.csv', header=0,usecols=['Passengers'])
X = series.values
train_size = int(len(X) * 0.66)
train, test = X[0:train_size], X[train_size:len(X)]
print('Observations: %d' % (len(X)))
print('Training Observations: %d' % (len(train)))
print('Testing Observations: %d' % (len(test)))
pyplot.plot(train); 
pyplot.plot([None for i in train] + [x for x in test])
pyplot.show()

#Esempio di Train-Test su FilRouge
import pandas as pd, os
from matplotlib import pyplot
os.chdir('/Users/marco/Documents/SSD_Project/SSD_Python/decomposition_example')
series = pd.read_csv('FilRouge.csv', header=0,usecols=['sales'])
X = series.values
train_size = int(len(X) - 4)
train, test = X[0:train_size], X[train_size:len(X)]
print('Observations: %d' % (len(X)))
print('Training Observations: %d' % (len(train)))
print('Testing Observations: %d' % (len(test)))
pyplot.plot(train); 
pyplot.plot([None for i in train] + [x for x in test])
pyplot.show()

#Esempio di Log-Transform su BoxJenkins
import pandas as pd, os
import numpy as np
from matplotlib import pyplot
os.chdir('/Users/marco/Documents/SSD_Project/SSD_Python/decomposition_example')
series = pd.read_csv('BoxJenkins.csv', header=0,usecols=['Passengers'])
X = series.values
train_size = int(len(X) * 0.66)
train, test = X[0:train_size], X[train_size:len(X)]
print('Observations: %d' % (len(X)))
print('Training Observations: %d' % (len(train)))
print('Testing Observations: %d' % (len(test)))
pyplot.plot(train)
pyplot.plot([None for i in train] + [x for x in test])
y = X.transpose()[0] #l'array in input è multidimensionale, con transpose lo porto a monodimensionale.
x = np.arange(len(y))
z = np.polyfit(x, y, 2) # qui quadratico (1 lineare)
p = np.poly1d(z)
pyplot.plot(x,p(x),"r--")
pyplot.show()

# Serie storiche differenziate

import os
import numpy as np, pandas as pd, matplotlib.pyplot as plt
from statsmodels.graphics.tsaplots import plot_acf, plot_pacf
# Import data
os.chdir('/Users/marco/Documents/SSD_Project/SSD_Python/decomposition_example')
df = pd.read_csv('rawAirlinesPassengers.csv', usecols=[0], names=['value'], engine='python', header=0)
# Original Series
plt.rcParams.update({'figure.figsize':(9,7), 'figure.dpi':120})
fig, axes = plt.subplots(2, 2, sharex=True)
axes[0, 0].plot(df.value); axes[0, 0].set_title('Original Series')
plot_acf(df.value, ax=axes[0, 1])
# 1st Differencing
axes[1, 0].plot(df.value.diff()); axes[1, 0].set_title('1st Order Differencing')
plot_acf(df.value.diff().dropna(), ax=axes[1, 1])
plt.show()