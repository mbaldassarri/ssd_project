# -*- coding: utf-8 -*-
"""
Created on Wed Nov 20 16:39:10 2019

@author: Enrico
"""

# Sliding window MLP, Airline Passengers dataset (predicts t+1)
import os
import numpy as np, pandas as pd
import matplotlib.pyplot as plt
from tensorflow.keras.models import Sequential 
from tensorflow.keras.layers import Dense

# Funzione che verrrà chiamata sul TS della serie e la trasformerà in insieme
# di record della matrice che stò costruendo
# Crea le sliding windows
# npast = 1 dice quanti valori nel passato userò per fare la previsione nel futuro
# from series of values to windows matrix
def compute_windows(nparray, npast=1):
    dataX, dataY = [], []  # window and value
    
    # Per tutti i sottoinsioni (windows) identifica la posizione di partenza 
    # "appendo" a X il valore a, "appendo" a Y il valore immediatamente successivo
    # Ogni window sarà lunga npast+ 1
    for i in range(len(nparray)-npast-1):
        a = nparray[i:(i+npast), 0]
        dataX.append(a)
        dataY.append(nparray[i + npast, 0])
        return np.array(dataX), np.array(dataY)
    
np.random.seed(550) # for reproducibility
os.chdir('/Users/marco/Documents/SSD_Project/SSD_Python/neural_networks')
df = pd.read_csv('result_set.csv', usecols=[0],
                 names=['value'], header=0)
dataset = df.values # time series values
dataset = dataset.astype('float32')  # needed for MLP input

# train - test sets
cutpoint  = int(len(dataset) * 0.7)
#
# 70% train, 30% test
#
train, test = dataset[:cutpoint], dataset[cutpoint:]
print("Len train={0}, len test={1}".format(len(train), len(test)))
# sliding window matrices (npast = window width); dim = n - npast - 1
npast = 3
trainX, trainY = compute_windows(train, npast)
testX, testY   = compute_windows(test, npast)
# should get also the last npred of train
# Multilayer Perceptron model
model = Sequential()
n_hidden = 8
n_output = 1
# Aggiunge allo strato di input uno strato di tipo Dense
model.add(Dense(n_hidden, input_dim=npast, activation='relu'))

# Aggiunge allo strato hidden uno strato di tipo Dense
# hidden neurons, 1 layer
model.add(Dense(n_output))
# output neurons
# adam tipo di ottimizzatore del gradiente (nome dell'ideatore)
model.compile(loss='mean_squared_error', optimizer='adam')

# batch size partiziona il TS e propone di apprendere su un insieme di 10 record
model.fit(trainX, trainY, epochs=200, batch_size=10, verbose=2)
# batch_size divisor of len(trainX)

# Model performance
trainScore = model.evaluate(trainX, trainY, verbose=0)
print('Score on train: MSE = {0:0.2f} '.format(trainScore))
testScore = model.evaluate(testX, testY, verbose=0)
print('Score on test:  MSE = {0:0.2f} '.format(testScore))

trainPredict = model.predict(trainX)    # predictions
testForecast = model.predict(testX)     # forecast

plt.rcParams["figure.figsize"] = (10,8) # redefines figure size
plt.plot(dataset)
plt.plot(np.concatenate((np.full(1,np.nan),trainPredict[:,0])))
plt.plot(np.concatenate((np.full(len(train)+1,np.nan), testForecast[:,0])))
plt.show()





# LSTM Example
import pandas as pd, numpy as np, os
import matplotlib.pyplot as plt
os.chdir('/Users/marco/Documents/SSD_Project/SSD_Python/neural_networks')
df = pd.read_csv('gioiellerie.csv', header=0)
df["period"] = df["year"].map(str) +"-" + df["month"].map(str)
df['period'] = pd.to_datetime(df['period'], format="%Y-%m").dt.to_period('M')
df.set_index('period')
aSales = df['sales'].to_numpy() # array of sales data
logdata = np.log(aSales) # log transform
data = pd.Series(logdata) # convert to pandas series
plt.rcParams["figure.figsize"] = (10,8) # redefines figure size
plt.plot(data.values);
plt.show # data plot
train = data[:-12]
test = data[-12:]
reconstruct = np.exp(np.r_[train,test]) # simple recosntruction

# ------------------------------------------------- neural forecast
from sklearn.preprocessing import MinMaxScaler
scaler = MinMaxScaler()
scaler.fit_transform(train.values.reshape(-1, 1))
scaled_train_data = scaler.transform(train.values.reshape(-1, 1))
scaled_test_data = scaler.transform(test.values.reshape(-1, 1))
from keras.preprocessing.sequence import TimeseriesGenerator
n_input = 12; n_features = 1
generator = TimeseriesGenerator(scaled_train_data, scaled_train_data,
length=n_input, batch_size=1)
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import LSTM
lstm_model = Sequential()
lstm_model.add(LSTM(20, activation='relu', input_shape=(n_input, n_features),
dropout=0.05))
lstm_model.add(Dense(1))
lstm_model.compile(optimizer='adam', loss='mse')
lstm_model.summary()
lstm_model.fit_generator(generator,epochs=25)

losses_lstm = lstm_model.history.history['loss']
plt.xticks(np.arange(0,21,1)) # convergence trace
plt.plot(range(len(losses_lstm)),losses_lstm); 
plt.show
lstm_predictions_scaled = list()
batch = scaled_train_data[-n_input:]
curbatch = batch.reshape((1, n_input, n_features))
for i in range(len(test)):
    lstm_pred = lstm_model.predict(curbatch)[0]
    lstm_predictions_scaled.append(lstm_pred)
    curbatch = np.append(curbatch[:,1:,:],[[lstm_pred]],axis=1)
lstm_forecast = scaler.inverse_transform(lstm_predictions_scaled)
yfore = np.transpose(lstm_forecast).squeeze()
# recostruction
expdata = np.exp(train) # unlog
expfore = np.exp(yfore)
plt.plot(df.sales, label="sales")
plt.plot(expdata,label='expdata')
plt.plot([None for x in expdata]+[x for x in expfore], label='forecast')
plt.legend()
plt.show



    