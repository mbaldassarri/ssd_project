import numpy as np
import pandas as pd
from scipy import stats # to be used later
import matplotlib.pyplot as plt
import os
os.chdir('/Users/marco/Documents/SSD_Project/SSD_Python/traffic_example')
df = pd.read_csv('traffico16.csv') # dataframe (series)
npa = df['ago-01'].to_numpy() # numpy array

plt.hist(npa, bins=10, color='#00AA00', edgecolor='black')
plt.title(df.columns[0]);plt.xlabel('num');plt.ylabel('days')
plt.show()


print("\n Media: {0}".format(np.mean(npa)))
print("\n Mediana: {0}".format(np.median(npa)))
print("\n Moda: {0}".format(stats.mode(npa)[0]))


print( df['ago-02'].describe() )

#boxplot: si usano per avere una visione grafica di quartili, mediana, max e min
print('boxplot')
df.boxplot(column=['ago-02'])
plt.boxplot(npa)

#boxplot che confronta i valori delle serie storiche di tutti i mesi
df.boxplot(column=['ago-01', 'ago-02','set-01', 'set-02', 'ott-01', 'ott-02'])
plt.boxplot(npa)

print(pd.Series([1,2,3]).describe())

